package com.eltex.lab_02;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.TreeSet;
import java.util.UUID;

public class ShoppingCart {

    private LinkedList<Equipment> shoppingList = new LinkedList<>();
    private TreeSet<UUID> idSet = new TreeSet<>();


    /**
     * This void is for adding chosen Equipment in LinkedList
     * Also for adding Equipment's Id in TreeSet
     */
    public void add (Equipment equpment){

        shoppingList.add(equpment);

        UUID id = equpment.getId();
        idSet.add(id);

    }

    /**
     * This void is for delete chosen Equipment from LinkedList
     * Also for delete Equipment's Id from TreeSet
     */
    public void delete (Equipment equpment){

        shoppingList.remove(equpment);

        UUID id = equpment.getId();
        idSet.remove(id);

    }


    /**
     * Void for demonstration fo all equipment from LinkedList
     */
    public void showAll(){
        for (Equipment e: shoppingList) {
           System.out.println(e);
        }
    }

    /**
     * Void for searching Equipment by Id
     * @param id
     */
    public void findById (UUID id){
       if (idSet.contains(id)){
           ListIterator<Equipment> iter = shoppingList.listIterator();
           while (iter.hasNext()){
               Equipment obj = iter.next();
               if(obj.getId().equals(id)){
                   System.out.println(obj);
                   break;
               }
           }
       } else {
           System.out.println("There in no equipment with such Id!");
       }
    }

}
