package com.eltex.lab_02;


public class Console extends Equipment {

    String generation;                  // variables for object's parametr

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    Console() {
        super();
        generation = "Fourth";
    }

    Console(String generation) {
        this.generation = generation;
    }

    public void create() {
        super.create();
        int gen = getRandom().nextInt(4);
        switch (gen) {
            case 0:
                generation = "First generation";
                break;
            case 1:
                generation = "Second generation";
                break;
            case 2:
                generation = "Third generation";
                break;
            case 3:
                generation = "Fourth generation";
                break;
        }
    }

    public void read() {
        super.read();
        System.out.println("Generation: " + generation);

    }

    public void update() {
        super.update();
        System.out.println("enter generation of console");
        do {
            generation = getScanner().nextLine();
        } while (generation.isEmpty());
    }

    @Override
    public void delete() {
        super.delete();
        generation = null;
    }

    public String toString (){
        return super.toString() + "    Generation: " + generation;
    }

}
