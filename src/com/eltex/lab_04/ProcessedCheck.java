package com.eltex.lab_04;

public class ProcessedCheck extends ACheck{

    public ProcessedCheck(Orders orders, int numberOfChecks, long time){
        super(orders, numberOfChecks, time);
    }

    @Override
    public void run() {
        for (int i = 0 ; i < numberOfChecks ; i++) {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            orders.checkOrders();
            System.out.println("Processed orders has been deleted.");
            System.out.println(" ");
        }
    }

}
