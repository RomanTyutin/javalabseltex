package com.eltex.lab_04;

import java.util.ArrayList;
import java.util.Random;


public class OrderMaker extends Thread {

    private Orders orders;
    private int numberOfOrders;
    private long timeInterval;
    private Random random = new Random();



    public OrderMaker(Orders orders, int numberOfOrders, long timeInterval) {
        this.orders = orders;
        this.numberOfOrders = numberOfOrders;
        this.timeInterval = timeInterval;
    }

    ;


    @Override
    public void run() {
        makeRandomOrder();
    }

    public void makeRandomOrder() {
        for (int i = 0; i < numberOfOrders; i++) {

            Equipment equipment = null;
            int type = random.nextInt(3);
            if (type == 0) {
                equipment = new TV();
            } else if (type == 1) {
                equipment = new SetTopBox();
            } else {
                equipment = new Console();
            }

            equipment.create();

            ShoppingCart cart = new ShoppingCart();
            cart.add(equipment);

            Credentials customer = new Credentials();
            customer.setRandomCustomersData();

            Order order = new Order(customer, cart);


                orders.makeBuy(order);
                //orders.showAll();




            try {
                Thread.sleep(timeInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
