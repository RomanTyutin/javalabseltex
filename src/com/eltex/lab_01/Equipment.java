package com.eltex.lab_01;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;


public abstract class Equipment implements ICrudAction {

    UUID id;                                    // variables for object's parameter
    private int price;
    private String firm;
    private String model;
    private String name;


    private Random random = new Random();       // variables for void's realization
    private Scanner scanner = new Scanner(System.in);

    public Random getRandom() {
        return random;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public void create() {
        id = UUID.randomUUID();
        price = random.nextInt(100000);
        int firm_rand = random.nextInt(2);
        switch (firm_rand) {
            case 0:
                firm = "Sony";
                model = "0001";
                break;
            case 1:
                firm = "Microsoft";
                model = "1000";
                break;
        }
        name = getClass().getSimpleName();
    }

    @Override
    public void read() {

        System.out.println("ID: " + id);
        System.out.println("Price: " + price);
        System.out.println("Firm: " + firm);
        System.out.println("Model: " + model);
        System.out.println("Name: " + name);
    }

    @Override
    public void update() {
        boolean id_input;
        do {
            id_input = false;
            try {
                System.out.println("enter ID (in format: 00000000-0000-0000-0000-000000000000)");
                String command = scanner.nextLine();
                id = UUID.fromString(command);
            } catch (IllegalArgumentException e) {
                // System.out.println("enter ID (in format: 00000000-0000-0000-0000-000000000000)");
                id_input = true;
            }
        } while (id_input);

        boolean price_input;
        do {
            price_input = false;
            try {
                System.out.println("enter price");
                Scanner scanner = new Scanner(System.in);
                price = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter price as a integer, please!");
                price_input = true;
            }
        } while (price_input);


        System.out.println("enter firm");
        firm = scanner.nextLine();
        System.out.println("enter model");
        model = scanner.nextLine();
        name = getClass().getSimpleName();
    }

    @Override
    public void delete() {
        id = null;
        price = 0;
        firm = null;
        model = null;
        name = null;
    }
}
