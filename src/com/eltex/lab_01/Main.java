package com.eltex.lab_01;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        try {

            try {


                int quantity = Integer.parseInt(args[0]);
                System.out.println("Quantity of new equipment is " + quantity);
                if ("TV".equalsIgnoreCase(args[1]) || "Console".equalsIgnoreCase(args[1]) || "Box".equalsIgnoreCase(args[1])) {
                    System.out.println("Type of new equipment is " + args[1]);
                } else {
                    System.out.println("Incorrect value of type!!! (Correct types are TV/Console/Box)");
                    System.exit(0);
                }
                LinkedList<Equipment> goods = new LinkedList<>();


                int i = 0;
                while (i < quantity) {


                    Equipment object = null;


                    if ("TV".equalsIgnoreCase(args[1])) {
                        object = new TV();
                    } else if ("Console".equalsIgnoreCase(args[1])) {
                        object = new Console();
                    } else if ("Box".equalsIgnoreCase(args[1])) {
                        object = new SetTopBox();
                    }


                    boolean next_obj = false;
                    int j = 0;
                    while (j < 1) {


                        Scanner scanner = new Scanner(System.in);
                        System.out.println("Enter command, please (create/read/update/delete to edit the edit and OK " +
                                "to finish editing the object)");
                        String command = scanner.nextLine();
                        //System.out.println(command);

                        //try {
                        if ("create".equalsIgnoreCase(command) || "creat".equalsIgnoreCase(command) ||
                                "crea".equalsIgnoreCase(command) || "cre".equalsIgnoreCase(command) ||
                                "cr".equalsIgnoreCase(command) || "c".equalsIgnoreCase(command)) {
                            object.create();
                            System.out.println("created");
                        } else if ("read".equalsIgnoreCase(command) || "rea".equalsIgnoreCase(command) ||
                                "re".equalsIgnoreCase(command) || "r".equalsIgnoreCase(command)) {
                            object.read();
                            System.out.println("Count " + (i + 1));
                            System.out.println("read");
                        } else if ("update".equalsIgnoreCase(command) || "updat".equalsIgnoreCase(command) ||
                                "upda".equalsIgnoreCase(command) || "upd".equalsIgnoreCase(command) ||
                                "up".equalsIgnoreCase(command) || "u".equalsIgnoreCase(command)) {
                            object.update();
                            System.out.println("updated");
                        } else if ("delete".equalsIgnoreCase(command) || "delet".equalsIgnoreCase(command) ||
                                "dele".equalsIgnoreCase(command) || "del".equalsIgnoreCase(command) ||
                                "de".equalsIgnoreCase(command) || "d".equalsIgnoreCase(command)) {
                            object.delete();
                            System.out.println("deleted");
                        } else if ("ok".equalsIgnoreCase(command) || "o".equalsIgnoreCase(command)) {
                            j++;
                            next_obj = true;
                            System.out.println("This object has been edited!");
                        } else {
                            System.out.println("unknown command");
                        }
//                    } catch (NullPointerException e) {
//                        System.out.println("incorrect value of type!!!");
//                    }

                    }

                    if (next_obj == true) {
                        i++;
                    }

                    goods.add(object);

                }
                System.out.println(goods);

            } catch (NumberFormatException e) {
                System.out.println("First argument should be integer!");
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("To start this program you should enter quantity of goods and type of goods " +
                    "(TV/Console/Box)");
        }
    }

}
