package com.eltex.lab_03;

public interface ICrudAction {
    void create();
    void read();
    void update();
    void delete();
}
