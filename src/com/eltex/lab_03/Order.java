package com.eltex.lab_03;

import java.time.LocalDateTime;

public class Order implements Comparable<Order>{

    private String status;

    private LocalDateTime creationMoment;
    private static long WAITING_TIME = 5;


    private Credentials customer;
    private ShoppingCart cart;

    @Override
    public int compareTo(Order otherOrder){
        return this.creationMoment.compareTo(otherOrder.creationMoment);
    }


    public Order(Credentials customer, ShoppingCart cart) {
        this.customer = customer;
        this.cart = cart;
        this.status = "Awating";
        this.creationMoment = LocalDateTime.now();
    }


    public LocalDateTime getCreationMoment() {
        return creationMoment;
    }


    public static long getWaitingTime() {
        return WAITING_TIME;
    }


    public String getStatus() {
        return status;
    }


    public void hasBeenProcessed() {
        status = "Order has been proccesed.";
    }

    public void showInfo (){
        System.out.println("Customer: ");
        System.out.println(customer);
        System.out.println("Purchase time: " + creationMoment);
        System.out.println("Purchase status: " + status);
        System.out.println("Equipment: ");
        cart.showAll();
    }

}
