package com.eltex.lab_03;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TV extends Equipment {

    private int diagonal_size;                  // variables for object's parametr

    public int getDiagonal_size() {
        return diagonal_size;

    }

    public void setDiagonal_size(int diagonal_size) {
        this.diagonal_size = diagonal_size;
    }


    TV() {
        super();
        diagonal_size = 80;

    }

    TV(int diagonal_size) {
        this.diagonal_size = diagonal_size;
    }

    public void create() {
        super.create();
        diagonal_size = getRandom().nextInt(200);
    }

    public void read() {
        super.read();
        System.out.println("Diagonal size: " + diagonal_size);
    }

    public void update() {
        super.update();

        boolean diagonal_input;
        do {
            diagonal_input = false;
            try {
                System.out.println("enter diagonal size of TV");
                Scanner scanner = new Scanner(System.in);
                diagonal_size = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter price as a integer, please!");
                diagonal_input = true;
            }
        } while (diagonal_input);
    }

    @Override
    public void delete() {
        super.delete();
        diagonal_size = 0;
    }

    public String toString (){
        return super.toString() + "    Diagonal size: " + diagonal_size;
    }

}

