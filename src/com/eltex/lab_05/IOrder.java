package com.eltex.lab_05;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.UUID;

public interface IOrder {
    Order readById(UUID id, String fileName);
    void saveById(UUID id, String fileName);
    PriorityQueue readAll(String fileName);
    void saveAll(String fileName);
}
