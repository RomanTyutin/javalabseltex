package com.eltex.lab_05;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.PriorityQueue;

public class Orders<T extends Order> {



    private PriorityQueue<T> ordersQueue = new PriorityQueue<>();
    private HashMap<LocalDateTime, T> ordersTimeSet = new HashMap<>();

    public synchronized PriorityQueue<T> getOrdersQueue() {
        return ordersQueue;
    }

    public synchronized void makeBuy(T t) {

        ordersTimeSet.put(t.getCreationMoment(), t);
        ordersQueue.add(t);

    }

    public synchronized void changeOrdersStatus() {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {

            Entry<LocalDateTime, T> entry = iter.next();

            if ("Awating".equals(entry.getValue().getStatus())) {
                entry.getValue().hasBeenProcessed();
            }
        }
    }


    public synchronized void checkOrders() {

        Iterator<Entry<LocalDateTime, T>> iter = ordersTimeSet.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<LocalDateTime, T> entry = iter.next();

            if (/*entry.getKey().compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
                    ||*/ "Order has been processed.".equals(entry.getValue().getStatus())) {
                ordersQueue.remove(entry.getValue());
                iter.remove();
            }

        }


/*        ordersTimeSet.forEach((k, v) ->
        {
            if (k.compareTo(LocalDateTime.now().minusSeconds(Order.getWaitingTime())) < 0
            || "Order has been processed.".equals(v.getStatus())) {
                ordersQueue.remove(v);
                ordersTimeSet.remove(k);
            }
        });*/

    }


    public synchronized void showAll() {

        ordersTimeSet.forEach((k, v) -> v.showInfo());
        if (ordersTimeSet.isEmpty()) {
            System.out.println("There are no orders!");
        }
        System.out.println("Map's size: " + ordersTimeSet.size());
        System.out.println("Queue's size: " + ordersQueue.size());
        System.out.println(" ");
    }

}
