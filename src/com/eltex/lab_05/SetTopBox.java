package com.eltex.lab_05;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SetTopBox extends Equipment {

    int channels_quantity;                  // variables for object's parametr

    public int getChannels_quantity() {
        return channels_quantity;
    }

    public void setChannels_quantity(int channels_quantity) {
        this.channels_quantity = channels_quantity;
    }

    SetTopBox() {
        super();
        channels_quantity = 100;
    }

    SetTopBox(int channels_quantity) {
        this.channels_quantity = channels_quantity;
    }

    public void create() {
        super.create();
        channels_quantity = getRandom().nextInt(100);
    }

    public void read() {
        super.read();
        System.out.println("Channel's quantity: " + channels_quantity);
    }

    public void update() {
        super.update();

        boolean channels_quantity_input;
        do {
            channels_quantity_input = false;
            try {
                System.out.println("enter quantity of channels");
                Scanner scanner = new Scanner(System.in);
                channels_quantity = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter quantity of channels as a integer, please!");
                channels_quantity_input = true;
            }
        } while (channels_quantity_input);
    }

    @Override
    public void delete() {
        super.delete();
        channels_quantity = 0;
    }

    public String toString (){
        return super.toString() + "    Quantity of channels: " + channels_quantity;
    }

}
