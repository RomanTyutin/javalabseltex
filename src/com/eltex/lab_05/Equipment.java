package com.eltex.lab_05;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;


public abstract class Equipment implements ICrudAction, Serializable {


    private UUID id;                                    // variables for object's parameter
    private int price;
    private String firm;
    private String model;
    private String name;
    private static int count = 0;


    private Random random = new Random();       // variables for void's realization
    private transient Scanner scanner = new Scanner(System.in);

    public Equipment(){
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Random getRandom() {
        return random;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getCount(){ return count;}


    @Override
    public void create() {
        price = random.nextInt(100000);
        int firm_rand = random.nextInt(2);
        switch (firm_rand) {
            case 0:
                firm = "Sony";
                model = "0001";
                break;
            case 1:
                firm = "Microsoft";
                model = "1000";
                break;
        }
        name = getClass().getSimpleName();
        count++;
    }

    @Override
    public void read() {

        System.out.println("ID: " + id);
        System.out.println("Price: " + price);
        System.out.println("Firm: " + firm);
        System.out.println("Model: " + model);
        System.out.println("Name: " + name);
    }

    @Override
    public void update() {

        boolean price_input;
        do {
            price_input = false;
            try {
                System.out.println("enter price");
                Scanner scanner = new Scanner(System.in);
                price = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Enter price as a integer, please!");
                price_input = true;
            }
        } while (price_input);

        System.out.println("enter firm");
        do {
            firm = scanner.nextLine();
        } while (firm.isEmpty());

        System.out.println("enter model");
        do{
        model = scanner.nextLine();
        } while (model.isEmpty());

        name = getClass().getSimpleName();
    }

    @Override
    public void delete() {
        price = 0;
        firm = null;
        model = null;
        name = null;
        count--;
    }

    public String toString (){
        return "Equipment's type: " + name +
                "    Produced by: " + firm +
                "    Model: " + model +
                "    Price: " + price +
                "    Id: " + id;
    }
}
