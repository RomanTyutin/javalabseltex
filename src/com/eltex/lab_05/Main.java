package com.eltex.lab_05;


import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        Orders orders = new Orders();


        new OrderMaker(orders, 2, 0).start();

        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        orders.showAll();

        ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders);
        managerOrderFile.saveAll("filezzz");


        System.out.println("Queue's size" + orders.getOrdersQueue().size());


        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter id of order to be saved");
        UUID id = UUID.fromString(scanner.nextLine());
        managerOrderFile.saveById(id, "filezzz222");

        System.out.println("From filezzz by readAll void");
        PriorityQueue<Order> queue = managerOrderFile.readAll("filezzz");
        for (Order o: queue){
            o.showInfo();
        }


        Order deciredOrder = managerOrderFile.readById(id, "filezzz222");
        System.out.println(" ");
        System.out.println("From filezzz222 by readById void:");
        deciredOrder.showInfo();

    }

}
